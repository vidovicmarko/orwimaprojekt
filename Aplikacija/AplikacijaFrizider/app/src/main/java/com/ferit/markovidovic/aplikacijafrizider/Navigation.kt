package com.ferit.markovidovic.aplikacijafrizider

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.ferit.markovidovic.aplikacijafrizider.data.GroceryViewModel
import com.ferit.markovidovic.aplikacijafrizider.home.AddGroceryScreen
import com.ferit.markovidovic.aplikacijafrizider.home.GroceryScreen
import com.ferit.markovidovic.aplikacijafrizider.home.HomeScreen


object Routes {
    const val SCREEN_HOME_SCREEN = "groceryList"
    const val SCREEN_GROCERY_DETAILS = "groceryDetails/{groceryId}"
    const val SCREEN_ADD_GROCERY = "groceryAdd"

    fun getGroceryDetailsPath(groceryId: String?): String{
        if(groceryId != null){
            return "groceryDetails/$groceryId"
        }
        return "groceryDetails/0"
    }
}

@Composable
fun NavigationController(
    viewModel: GroceryViewModel
) {
    val navController = rememberNavController()

    NavHost(
        navController = navController, startDestination =
        Routes.SCREEN_HOME_SCREEN
    ) {
        composable(Routes.SCREEN_HOME_SCREEN) {
            HomeScreen(
                navigation = navController,
                viewModel = viewModel
            )
        }
        composable(
            Routes.SCREEN_GROCERY_DETAILS,
            arguments = listOf(
                navArgument("groceryId") {
                    type = NavType.StringType
                }
            )
        ) { backStackEntry ->
            val groceryId = backStackEntry.arguments?.getString("groceryId")
            val grocery = viewModel.groceryData.firstOrNull { it.id == groceryId }

            if (grocery != null) {
                GroceryScreen(
                    grocery = grocery,
                    navigation = navController,
                    viewModel = viewModel
                )
            } else {
            }
        }

        composable(Routes.SCREEN_ADD_GROCERY){
                AddGroceryScreen(
                    navigation = navController,
                    viewModel = viewModel
                )
            }

    }
}