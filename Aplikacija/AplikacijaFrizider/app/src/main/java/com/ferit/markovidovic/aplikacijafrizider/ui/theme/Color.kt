package com.ferit.markovidovic.aplikacijafrizider.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

val DarkGray = Color(0xFF1B1B1B)
val White = Color(0xFFFFFFFF)
val LightGray = Color(0xFFF7F7F7)
val DarkGreen = Color(0xFF13341E)
val LightGreen = Color(0xFF4E7145)
val Yellow = Color(0xFFEB9C35)
val Orange = Color(0xFFC4501B)
val RedBrown = Color(0xFF8A2B13)
val veryLightGreen = Color(0xFFDAF3D4)
val DarkBlue = Color(0xFF0E263B)
val LightBlue = Color(0xFFD0F4FF)