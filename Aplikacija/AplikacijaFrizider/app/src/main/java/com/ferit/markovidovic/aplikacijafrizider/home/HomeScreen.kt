package com.ferit.markovidovic.aplikacijafrizider.home

import androidx.compose.foundation.background
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import com.ferit.markovidovic.aplikacijafrizider.R
import com.ferit.markovidovic.aplikacijafrizider.Routes
import com.ferit.markovidovic.aplikacijafrizider.data.Category
import com.ferit.markovidovic.aplikacijafrizider.data.Grocery
import com.ferit.markovidovic.aplikacijafrizider.data.GroceryViewModel
import com.ferit.markovidovic.aplikacijafrizider.ui.theme.DarkGreen
import com.ferit.markovidovic.aplikacijafrizider.ui.theme.LightGreen
import com.ferit.markovidovic.aplikacijafrizider.ui.theme.White
import com.ferit.markovidovic.aplikacijafrizider.ui.theme.Yellow
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

@Composable
fun HomeScreen(
    navigation: NavController,
    viewModel: GroceryViewModel
){

    var groceries by remember { mutableStateOf<List<(Grocery)>>(viewModel.groceryData)}

    var filteredGroceries by remember { mutableStateOf<List<Grocery>>(viewModel.groceryData)}

    var currentCategoryId by remember { mutableStateOf(0) }
    var searchInput by remember { mutableStateOf("")}
    val currentTime = remember { mutableStateOf(Calendar.getInstance().time) }
    val formattedTime = SimpleDateFormat("HH:mm", Locale.getDefault()).format(currentTime.value)

    Column(
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
    ) {

        ScreenTitle(
            "MOBILE FRIDGE",
            greetingMessage(formattedTime)
        )
        SearchBar(
            viewModel,
            onSearch = { filteredGroceries = it }
        )

        GroceryCategories { categoryId ->
            currentCategoryId = categoryId
        }

        Column(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth()
        ) {
            GroceryList(currentCategoryId, searchInput, filteredGroceries, navigation)
        }

        GroceryAddButton(
            navigation
        )
    }
}

@Composable
fun ScreenTitle(
    title:  String,
    subtitle: String
){
    Box(
        modifier = Modifier
            .padding(20.dp)
            .fillMaxWidth(),
        contentAlignment = Alignment.TopStart
    ){
        Text(
            text = subtitle,
            style = TextStyle(color = LightGreen, fontSize = 22.sp, fontWeight = FontWeight.Medium, fontStyle = FontStyle.Italic),
            modifier = Modifier.padding(top = 20.dp)
        )
        Text(
            text = title,
            style = TextStyle(color = DarkGreen, fontSize = 30.sp, fontWeight = FontWeight.Bold),
            modifier = Modifier.padding(top = 50.dp),
        )
    }
}

@Composable
fun greetingMessage(currentTime: String): String {
    return when {
        currentTime in "00:01".."10:00" -> "Good Morning!"
        currentTime < "18:00" -> "Good Day!"
        else -> "Good Evening!"
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchBar(
    viewModel: GroceryViewModel,
    onSearch: (List<Grocery>) -> Unit
){
    var searchInput = remember { mutableStateOf("")}

    TextField(
        value = searchInput.value,
        onValueChange = { newValue ->
            searchInput.value = newValue
            val filteredGroceries = viewModel.groceryData.filter { grocery ->
                grocery.title.contains(newValue, ignoreCase = true)
            }
            onSearch(filteredGroceries)
        },
        label = { Text("Search")},
        leadingIcon = {
            Icon(
                painter = painterResource(id = R.drawable.search),
                contentDescription = null,
                modifier = Modifier
                    .width(22.dp)
            )
        },
        colors = TextFieldDefaults.textFieldColors(
            containerColor = LightGreen,
            unfocusedLabelColor = White,
            focusedLabelColor = White,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent,
            unfocusedTextColor = White,
            focusedTextColor = White
        ),
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 20.dp, vertical = 2.dp)
            .clip(RoundedCornerShape(10.dp))
    )
}


@Composable
fun TabButton(
    text: String,
    isActive: Boolean,
    onClick: () -> Unit
)   {
    Button(
        shape = RoundedCornerShape(10.dp),
        elevation = null,
        colors = if (isActive) ButtonDefaults.buttonColors(contentColor = Yellow, containerColor = Color.Black) else
            ButtonDefaults.buttonColors(contentColor = Yellow, containerColor = DarkGreen),
        modifier = Modifier
            .fillMaxHeight()
            .padding(horizontal = 5.dp),
        onClick = { onClick() }
    ){
        Text(
            text,
            style = TextStyle(fontSize = 16.sp)
            )
    }
}

@Composable
fun GroceryCategories(
    onCategorySelected: (Int) -> Unit
){

    val categories = listOf(
        Category(id = 0, name = "All"),
        Category(id = 1, name = "Fruits"),
        Category(id = 2, name = "Vegetables"),
        Category(id = 3, name = "Meat"),
        Category(id = 4, name = "Milk products"),
        Category(id = 5, name = "Drinks"),
        Category(id = 6, name = "Other")
    )

    var currentActiveCategory by remember { mutableStateOf(categories.first()) }

    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .padding(vertical = 16.dp)
            .background(Color.Transparent)
            .fillMaxWidth()
            .height(44.dp)
            .horizontalScroll(rememberScrollState())
    ){
        categories.forEach { category ->
        TabButton(text = category.name, isActive = currentActiveCategory == category) {
            currentActiveCategory = category
            onCategorySelected(category.id)
        }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun GroceryCard(
    grocery: Grocery,
    navigation: NavController
){
    val title: String = grocery.title
    val image: String = grocery.image
    val quantity: String = grocery.quantity
    val expiryDate: String = grocery.expiryDate

    Card(
        modifier = Modifier
            .padding(horizontal = 10.dp)
            .fillMaxWidth()
            .height(110.dp),
        shape = RectangleShape,
        onClick = {
            navigation.navigate(
                route = Routes.getGroceryDetailsPath(grocery.id)
            )
        }
    ){
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(
                    brush = Brush.horizontalGradient(
                        colors = listOf(
                            DarkGreen,
                            LightGreen
                        )
                    )
                )
                .padding(start = 5.dp),
            contentAlignment = Alignment.Center
        ){
            Row(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(horizontal = 10.dp),
                verticalAlignment = Alignment.CenterVertically
            ){
                Icon(
                    painter = rememberAsyncImagePainter(model = image),
                    contentDescription = "",
                    tint = Color.Unspecified,
                    modifier = Modifier
                        .width(90.dp)
                        .height(90.dp)
                )
                Spacer(modifier = Modifier.width(10.dp))
                Column {
                    Text(
                        text = title,
                        style = TextStyle(
                            color = Yellow,
                            fontSize = 20.sp,
                            fontWeight = FontWeight.Bold
                        )
                    )
                    Text(
                        text = "Quantity: $quantity",
                        style = TextStyle(
                            color = White,
                            fontSize = 20.sp
                        )
                    )
                    Text(
                        text = "Expiry date: $expiryDate",
                        style = TextStyle(
                            color = White,
                            fontSize = 20.sp
                        )
                    )
                }
            }
        }
    }
}

@Composable
fun GroceryList(
    categoryId: Int,
    searchInput: String,
    groceries: List<Grocery>,
    navigation: NavController
){
    val filteredGroceries = when (categoryId){
        0 -> groceries
        1 -> groceries.filter { it.categoryId == "Fruits" }
        2 -> groceries.filter { it.categoryId == "Vegetables" }
        3 -> groceries.filter { it.categoryId == "Meat" }
        4 -> groceries.filter { it.categoryId == "Milk products" }
        5 -> groceries.filter { it.categoryId == "Drinks" }
        6 -> groceries.filter { it.categoryId == "Other" }
        else -> emptyList()
    }.filter {
        it.title.contains(searchInput, ignoreCase = true)
    }

    if (filteredGroceries.isEmpty()) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = "No items available.",
                style = TextStyle(color = Color.Gray, fontSize = 20.sp),
                modifier = Modifier.wrapContentWidth(Alignment.CenterHorizontally)
            )
        }
    } else {
        LazyColumn (
        ){
            filteredGroceries.forEach { grocery ->
                item {
                    GroceryCard(grocery, navigation)
                    Spacer(modifier = Modifier.height(10.dp))
                }
            }
        }
    }
}

@Composable
fun GroceryAddButton(
    navigation: NavController
){
    Button(
        modifier = Modifier
            .fillMaxWidth()
            .padding(15.dp)
            .height(50.dp),
        onClick = {
            navigation.navigate(Routes.SCREEN_ADD_GROCERY)
        },
        colors = ButtonDefaults.buttonColors(
            containerColor = DarkGreen,
            contentColor = White
        )
    ) {
        Text(
            text = "Add new item",
            style = TextStyle(fontSize = 20.sp)
        )
    }
}
