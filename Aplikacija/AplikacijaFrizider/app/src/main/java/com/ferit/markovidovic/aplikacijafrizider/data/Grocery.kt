package com.ferit.markovidovic.aplikacijafrizider.data

import androidx.annotation.DrawableRes

data class Grocery(
    var id: String = "",
    var image: String = "",
    val title: String = "",
    val quantity:String = "",
    val additionDate : String = "",
    val expiryDate: String = "",
    val categoryId: String = ""
){}