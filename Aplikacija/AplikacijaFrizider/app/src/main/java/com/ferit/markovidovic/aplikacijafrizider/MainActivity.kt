package com.ferit.markovidovic.aplikacijafrizider

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.ferit.markovidovic.aplikacijafrizider.data.GroceryViewModel
import com.ferit.markovidovic.aplikacijafrizider.ui.theme.AplikacijaFriziderTheme
import com.google.firebase.FirebaseApp

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FirebaseApp.initializeApp(this)
        val viewModel = GroceryViewModel()
        setContent {
                NavigationController(viewModel = viewModel)

        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    AplikacijaFriziderTheme {
        Greeting("Android")
    }
}