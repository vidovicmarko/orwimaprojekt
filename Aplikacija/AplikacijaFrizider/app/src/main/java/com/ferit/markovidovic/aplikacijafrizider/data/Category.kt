package com.ferit.markovidovic.aplikacijafrizider.data

data class Category (
    val id: Int,
    val name: String
)
