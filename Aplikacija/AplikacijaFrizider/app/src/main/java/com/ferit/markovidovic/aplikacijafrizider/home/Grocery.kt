package com.ferit.markovidovic.aplikacijafrizider.home

import androidx.annotation.DrawableRes
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ButtonElevation
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import com.ferit.markovidovic.aplikacijafrizider.R
import com.ferit.markovidovic.aplikacijafrizider.Routes
import com.ferit.markovidovic.aplikacijafrizider.data.Grocery
import com.ferit.markovidovic.aplikacijafrizider.data.GroceryViewModel
import com.ferit.markovidovic.aplikacijafrizider.ui.theme.DarkBlue
import com.ferit.markovidovic.aplikacijafrizider.ui.theme.DarkGreen
import com.ferit.markovidovic.aplikacijafrizider.ui.theme.White

@Composable
fun GroceryScreen(
    grocery: Grocery,
    navigation: NavController,
    viewModel: GroceryViewModel
){
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(
                DarkBlue
            )
    ) {
        Column (
        ){
            BackButton(navigation, grocery.title)

            GroceryInfo(grocery.quantity , grocery.additionDate , grocery.expiryDate, grocery.categoryId, viewModel, grocery)

            Column (
                modifier = Modifier
                    .weight(1f)
                    .fillMaxWidth()
            ) {
                GroceryImage(grocery)
            }

            DeleteButton(
                onClick = {
                    viewModel.deleteDataFromFirestore(grocery)
                    navigation.popBackStack()
                }
            )
        }
    }
}

@Composable
fun BackButton(
    navigation: NavController,
    title: String
){

    Box(
        modifier = Modifier
            .padding(top = 30.dp, bottom = 20.dp)
            .fillMaxWidth(),
        contentAlignment = Alignment.TopCenter
    ){
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 20.dp)
        ) {
            CircularButton(
                iconResource = R.drawable.arrow,
                onClick = {
                    navigation.navigate(Routes.SCREEN_HOME_SCREEN)
                })
            Text(
                text = title,
                fontSize = 35.sp,
                color = White,
                fontWeight = FontWeight.Bold)
        }
    }
}

@Composable
fun CircularButton(
    @DrawableRes iconResource: Int,
    color: Color = Color.Black,
    elevation: ButtonElevation? =
        ButtonDefaults.buttonElevation(defaultElevation = 12.dp),
    onClick: () -> Unit = {}
){
    Button(
        contentPadding = PaddingValues(),
        elevation = elevation,
        onClick = { onClick() },
        colors = ButtonDefaults.buttonColors(containerColor = Color.Transparent, contentColor = color),
        shape = RoundedCornerShape(5.dp),
        modifier = Modifier
            .width(38.dp)
            .height(38.dp)
    ){
        Icon(
            painter = painterResource(id = iconResource),
            contentDescription = null,
            tint = White
        )
    }
}

@Composable
fun GroceryImage(
    grocery: Grocery
){
    Box(modifier = Modifier
        .fillMaxWidth()
        .width(150.dp)
        .height(200.dp)
        .background(Color.Transparent),
        contentAlignment = Alignment.Center
    ){
        Image(
            painter = rememberAsyncImagePainter(model = grocery.image),
            contentDescription = null,
            modifier = Modifier
                .fillMaxSize()
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun GroceryInfo(
    quantity: String,
    additionDate: String,
    expiryDate: String,
    category: String,
    viewModel: GroceryViewModel,
    grocery: Grocery
){
    Column (
        modifier = Modifier
            .fillMaxWidth()
            .padding(20.dp),
    ){
        Box(
            modifier = Modifier
                .height(65.dp)
                .background(Color.Transparent)
                .fillMaxWidth(),
            contentAlignment = Alignment.CenterStart
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                Spacer(modifier = Modifier.width(10.dp))
                Icon(
                    painter = painterResource(id = R.drawable.calendar),
                    contentDescription =null,
                    tint = White,
                    modifier = Modifier
                        .size(55.dp)
                )
                Spacer(modifier = Modifier.width(15.dp))
                Text(text = "Addition date: $additionDate",
                    fontSize = 20.sp,
                    color = White,
                    fontWeight = FontWeight.Bold)
            }
        }
        Spacer(modifier = Modifier.height(15.dp))
        Box(
            modifier = Modifier
                .height(65.dp)
                .background(Color.Transparent)
                .fillMaxWidth(),
            contentAlignment = Alignment.CenterStart
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                Spacer(modifier = Modifier.width(10.dp))
                Icon(
                    painter = painterResource(id = R.drawable.calendar),
                    contentDescription =null,
                    tint = White,
                    modifier = Modifier.size(55.dp)
                )
                Spacer(modifier = Modifier.width(15.dp))
                Text(text = "Expiry date: $expiryDate",
                    fontSize = 20.sp,
                    color = White,
                    fontWeight = FontWeight.Bold)
            }
        }
        Spacer(modifier = Modifier.height(15.dp))
        Box(
            modifier = Modifier
                .height(65.dp)
                .background(Color.Transparent)
                .fillMaxWidth(),
            contentAlignment = Alignment.CenterStart
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                Spacer(modifier = Modifier.width(11.dp))
                Icon(
                    painter = painterResource(id = R.drawable.quantity),
                    contentDescription =null,
                    tint = White,
                    modifier = Modifier.size(55.dp)
                )
                Spacer(modifier = Modifier.width(14.dp))
                
                var modifiedQuantity by remember { mutableStateOf(quantity.toInt()) }
                

                fun onQuantityChange(newQuantity: Int) {
                    modifiedQuantity = newQuantity
                    viewModel.groceryData.firstOrNull { it.id == grocery.id }?.let { existingGrocery ->
                        val updatedGrocery = existingGrocery.copy(quantity = newQuantity.toString())
                        viewModel.updateQuantityInFirestore(updatedGrocery)
                    }
                }

                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Center
                ) {
                    IconButton(onClick = {
                        if (modifiedQuantity > 0) {
                            onQuantityChange(modifiedQuantity - 1)
                        }
                    }) {
                        Icon(
                            painter = painterResource(id = R.drawable.minus),
                            contentDescription = null,
                            tint = White,
                            modifier = Modifier
                                .width(20.dp)
                        )
                    }
                    Spacer(modifier = Modifier.width(15.dp))

                    TextField(
                        value = modifiedQuantity.toString(),
                        onValueChange = {
                            onQuantityChange(it.toIntOrNull() ?: modifiedQuantity)
                        },
                        textStyle = TextStyle(
                            fontSize = 20.sp,
                            color = White,
                            fontWeight = FontWeight.Bold
                        ),
                        colors = TextFieldDefaults.textFieldColors(
                            containerColor = Color.Transparent,
                            focusedIndicatorColor = Color.Transparent,
                            unfocusedIndicatorColor = Color.Transparent
                        ),
                        modifier = Modifier.width(70.dp),
                        singleLine = true
                    )
                    Spacer(modifier = Modifier.width(10.dp))
                    IconButton(onClick = {
                        onQuantityChange(modifiedQuantity + 1)
                    }) {
                        Icon(
                            painter =  painterResource(id = R.drawable.plus),
                            contentDescription = null,
                            tint = White,
                            modifier = Modifier
                                .width(20.dp)
                        )
                    }
                }
            }
        }
        Spacer(modifier = Modifier.height(15.dp))
        Box(
            modifier = Modifier
                .height(65.dp)
                .background(Color.Transparent)
                .fillMaxWidth(),
            contentAlignment = Alignment.CenterStart
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                Spacer(modifier = Modifier.width(14.dp))
                Icon(
                    painter = painterResource(id = R.drawable.category),
                    contentDescription =null,
                    tint = White,
                    modifier = Modifier.size(50.dp)
                )
                Spacer(modifier = Modifier.width(14.dp))
                Text(
                    text = "Category: $category",
                    fontSize = 20.sp,
                    color = White,
                    fontWeight = FontWeight.Bold
                    )
            }
        }
        Spacer(modifier = Modifier.height(15.dp))
    }
}

@Composable
fun DeleteButton(
    onClick: () -> Unit
){
    Button(
        onClick = { onClick() },
        modifier = Modifier
            .fillMaxWidth()
            .padding(15.dp)
            .height(50.dp),
        colors = ButtonDefaults.buttonColors(
            contentColor = White,
            containerColor = DarkGreen
        )
    ) {
        Text(
            text = "Delete",
            style = TextStyle(fontSize = 20.sp)
        )
    }
}




