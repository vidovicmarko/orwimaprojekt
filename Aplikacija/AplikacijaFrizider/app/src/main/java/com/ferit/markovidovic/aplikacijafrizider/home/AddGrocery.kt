package com.ferit.markovidovic.aplikacijafrizider.home

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.ferit.markovidovic.aplikacijafrizider.data.Grocery
import com.ferit.markovidovic.aplikacijafrizider.data.GroceryViewModel
import com.ferit.markovidovic.aplikacijafrizider.ui.theme.DarkBlue
import com.ferit.markovidovic.aplikacijafrizider.ui.theme.DarkGreen
import com.ferit.markovidovic.aplikacijafrizider.ui.theme.LightGreen
import com.ferit.markovidovic.aplikacijafrizider.ui.theme.White


@Composable
fun AddGroceryScreen(
    navigation: NavController,
    viewModel: GroceryViewModel
){
    var name by remember { mutableStateOf("") }
    var dateAdded by remember { mutableStateOf("") }
    var expirationDate by remember { mutableStateOf("") }
    var quantity by remember { mutableStateOf("") }
    var category by remember { mutableStateOf("") }
    var image by remember { mutableStateOf("") }

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(
                DarkBlue
            )
    ) {
        Column(
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxSize()
        ) {

            BackButton(navigation, "")

            Column(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxWidth()
            ) {
                InformationChip("Item name:", name) { name = it }
                Spacer(Modifier.padding(5.dp))
                InformationChip("Addition date:", dateAdded) { dateAdded = it }
                Spacer(Modifier.padding(5.dp))
                InformationChip("Expiry date:", expirationDate) { expirationDate = it }
                Spacer(Modifier.padding(5.dp))
                InformationChip("Quantity:", quantity) { quantity = it }
                Spacer(Modifier.padding(5.dp))
                InformationChip("Category:", category) { category = it }
                Spacer(Modifier.padding(5.dp))
                InformationChip("Image:", image) { image = it }
            }

            AddButton(
                onClick = {
                    val defaultImage =
                        "https://firebasestorage.googleapis.com/v0/b/frizideraplikacija.appspot.com/o/food.png?alt=media&token=b80b73ac-f9a4-4c25-ad8e-8fbcb7744296"
                    val newGrocery = Grocery(
                        title = name,
                        quantity = quantity,
                        additionDate = dateAdded,
                        expiryDate = expirationDate,
                        categoryId = category,
                        image = if (image.isNotEmpty()) image else defaultImage
                    )

                    viewModel.addDataToFirestore(newGrocery)

                    navigation.popBackStack()
                },
            )
        }
    }
}



@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun InformationChip(
    text: String,
    value: String,
    onValueChange: (String) -> Unit
){
    Box(
        modifier = Modifier
            .height(70.dp)
            .fillMaxWidth()
            .background(
                color = Color.Transparent,
                shape = RoundedCornerShape(2.dp)
            ),
        contentAlignment = Alignment.Center
    ){
        Column (
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 10.dp)
        ){
            Row(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                TextField(
                    value = value,
                    onValueChange = {
                        onValueChange(it)
                    },
                    label = { Text(text) },
                    colors = TextFieldDefaults.textFieldColors(
                        unfocusedLabelColor = White,
                        focusedLabelColor = White,
                        containerColor = LightGreen,
                        unfocusedTextColor = White,
                        focusedTextColor = White,
                        focusedIndicatorColor = LightGreen,
                        unfocusedIndicatorColor = LightGreen
                    ),
                    modifier = Modifier
                        .fillMaxWidth()
                        .clip(RoundedCornerShape(10.dp))
                )
            }
        }
    }
}



@Composable
fun AddButton(
    onClick: () -> Unit
){
    Button(
        modifier = Modifier
            .fillMaxWidth()
            .padding(15.dp)
            .height(50.dp),
        onClick = { onClick() },
        colors = ButtonDefaults.buttonColors(
            containerColor = DarkGreen,
            contentColor = White
        )
    ) {
        Text(
            text = "Add",
            style = TextStyle(fontSize = 20.sp)
        )
    }
}

