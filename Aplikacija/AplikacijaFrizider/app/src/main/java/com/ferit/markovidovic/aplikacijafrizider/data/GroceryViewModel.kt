package com.ferit.markovidovic.aplikacijafrizider.data

import androidx.compose.runtime.mutableStateListOf
import com.google.firebase.Firebase
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.firestore

class GroceryViewModel {
    private val db = Firebase.firestore
    private val groceriesCollection: CollectionReference = db.collection("groceries")


    val groceryData = mutableStateListOf<Grocery>()

    init {
        fetchDatabaseData()
    }

    fun fetchDatabaseData(){
        db.collection("groceries")
            .get()
            .addOnSuccessListener {result ->
                for(data in result.documents){
                    val grocery = data.toObject(Grocery::class.java)
                    if (grocery != null){
                        grocery.id = data.id
                        groceryData.add(grocery)
                    }
                }
            }
    }

    fun addDataToFirestore(grocery: Grocery){
        val newDocumentReference = groceriesCollection.document()
        newDocumentReference
            .set(grocery, SetOptions.merge())
            .addOnSuccessListener {
                grocery.id = newDocumentReference.id
                groceryData.add(grocery)
            }
    }

    fun deleteDataFromFirestore(grocery: Grocery){
        val documentReference = groceriesCollection.document(grocery.id)
        documentReference
            .delete()
            .addOnSuccessListener {
                groceryData.remove(grocery)
            }
    }

    fun updateQuantityInFirestore(grocery: Grocery){
        val documentReference = groceriesCollection.document(grocery.id)
        documentReference
            .update("quantity", grocery.quantity)
            .addOnSuccessListener {
                val index = groceryData.indexOfFirst { it.id == grocery.id }
                if(index != -1){
                    groceryData[index] = grocery
                }
            }
    }
}